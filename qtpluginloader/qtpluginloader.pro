TARGET = qtpluginloader

TEMPLATE = lib
CONFIG += plugin
SOURCES = hd-plugin-loader-qt.c
HEADERS = hd-plugin-loader-qt.h

CONFIG += link_pkgconfig
PKGCONFIG += hildon-1 libhildondesktop-1

# install
target.path = /usr/lib/hildon-desktop/loaders
INSTALLS += target
