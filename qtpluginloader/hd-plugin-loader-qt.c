/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <libhildondesktop/libhildondesktop.h>

#include "hd-plugin-loader-qt.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <X11/Xlib.h>
#include <gdk/gdkx.h>

#ifndef HD_PLUGIN_MODULE_PATH
#define HD_PLUGIN_MODULE_PATH "/usr/lib/hildon-desktop"
#endif

/* those come from hd-config.h */
#define HD_PLUGIN_CONFIG_GROUP              "Desktop Entry"
#define HD_PLUGIN_CONFIG_KEY_TYPE           "Type"
#define HD_PLUGIN_CONFIG_KEY_PATH           "X-Path"
#define HD_PLUGIN_CONFIG_KEY_TEXT_DOMAIN    "X-Text-Domain"

#define QT_ABSTRACT_HOME_ITEM_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), QT_TYPE_ABSTRACT_HOME_ITEM, QtAbstractHomeItemPrivate))

struct _QtAbstractHomeItemPrivate
{
  pid_t pid; // process ID of child process

  GIOChannel *gio_read;

  gchar realized;
  gchar *executable_path;
};

G_DEFINE_TYPE (QtAbstractHomeItem, qt_abstract_home_item, HD_TYPE_HOME_PLUGIN_ITEM);

static void
qt_abstract_home_item_init (QtAbstractHomeItem *object)
{
    object->priv = QT_ABSTRACT_HOME_ITEM_GET_PRIVATE (object);

    QtAbstractHomeItemPrivate *priv = object->priv;
    priv->pid = 0;
    priv->gio_read = NULL;

    priv->realized = FALSE;
    priv->executable_path = 0;

}

static void
qt_abstract_home_item_finalize (GObject *object)
{
    QtAbstractHomeItemPrivate *priv = QT_ABSTRACT_HOME_ITEM (object)->priv;

    if (priv->gio_read)
    {
        g_io_channel_unref(priv->gio_read);
        priv->gio_read = NULL;
    }

    g_free (priv->executable_path);

    G_OBJECT_CLASS (qt_abstract_home_item_parent_class)->finalize (object);
}

static gboolean
qt_abstract_home_item_gio_in (GIOChannel *gio, GIOCondition condition, gpointer data)
{
    /*
       GIOStatus ret;
       gchar *msg;
       gsize len;
       GError *err = NULL; */
    GdkEvent *gdk_delete_event;
    QtAbstractHomeItem *object = data;
    GtkWidget *widget = GTK_WIDGET(object);

    (void) gio; // unused

    if (condition & G_IO_HUP) {
        /* send a delete event so that hildon-home get's notified that our item was closed */
        gdk_delete_event = gdk_event_new (GDK_DELETE);
        gdk_delete_event->any.window = widget->window;
        gdk_delete_event->any.send_event = TRUE;
        gtk_main_do_event (gdk_delete_event);
        gdk_event_free (gdk_delete_event);

        /* now reap the child process */
        QtAbstractHomeItem *object = (QtAbstractHomeItem *)data;
        waitpid( object->priv->pid, NULL, WNOHANG );
        return FALSE;
    }

    /* we are using the pipe only to check the life state of qt */
    /*
       ret = g_io_channel_read_line (gio, &msg, &len, NULL, &err);
       if (ret == G_IO_STATUS_ERROR)
       g_critical ("Error reading: %s", err->message);

       g_warning ("Read %u bytes: %s", len, msg);

       g_free (msg);
        */
    return TRUE;
}

static GError*
qt_abstract_home_item_error_new (const char *literal)
{
    GError *error = g_error_new_literal (g_quark_from_static_string("qt-loader-execute"),
            QT_LOADER_EXECUTE_ERROR,
            literal);
    return error;
}

static GError*
qt_abstract_home_item_execute (QtAbstractHomeItem *object)
{
    int qtToGtkPipe[2];
    int ret;
    QtAbstractHomeItemPrivate *priv = QT_ABSTRACT_HOME_ITEM (object)->priv;

    /* --- open the communication pipe */
    ret = pipe (qtToGtkPipe);
    if (ret == -1)
        return qt_abstract_home_item_error_new("Creating pipe failed.");

    /* --- Fork and exec the new application */
     priv->pid = fork();
     if (priv->pid < 0) {
        return qt_abstract_home_item_error_new("Unable to fork new process.");

    } else if (priv->pid) {
        /* I am the original process */
        close( qtToGtkPipe[1] );

        priv->gio_read = g_io_channel_unix_new (qtToGtkPipe[0]);
        if (!priv->gio_read)
            return qt_abstract_home_item_error_new("Cannot create new GIOChannel!");

        if (!g_io_add_watch (priv->gio_read, G_IO_IN | G_IO_HUP,
                    qt_abstract_home_item_gio_in, object))
            return qt_abstract_home_item_error_new("Cannot add watch on GIOChannel!");

    } else {
        /* I am the forked process */
        char *appletId;
        char writePipeStr[20];

        close( qtToGtkPipe[0] );

        appletId = hd_home_plugin_item_get_applet_id (HD_HOME_PLUGIN_ITEM (object));
        snprintf( writePipeStr, 19, "%d", qtToGtkPipe[1]);

        char *newargv[] = { priv->executable_path,
            "-plugin-id", appletId,
            "-write-pipe", writePipeStr,
            NULL };

        // note: old file descriptors are still open
        execv(newargv[0], newargv);

        // we should not run through here...
        g_free (appletId);
    }

    return 0;
}

static void
qt_abstract_home_item_realize (GtkWidget *widget)
{
    GdkDisplay *display;
    QtAbstractHomeItem *item = QT_ABSTRACT_HOME_ITEM (widget);
    QtAbstractHomeItemPrivate *priv = item->priv;

    GTK_WIDGET_CLASS (qt_abstract_home_item_parent_class)->realize (widget);

    /* Remove the applet id atom again. we have our own */
    display = gdk_drawable_get_display (widget->window);
    XDeleteProperty (GDK_WINDOW_XDISPLAY (widget->window),
                     GDK_WINDOW_XID (widget->window),
                     gdk_x11_get_xatom_by_name_for_display (display,
                                                            "_HILDON_APPLET_ID"));

    if (!priv->realized) {
        GError *error = NULL;
        priv->realized = TRUE;
        error = qt_abstract_home_item_execute (item);
        if (error) {
            g_warning ("%s", error->message);
            g_error_free (error);
        }
   }
}

static void
qt_abstract_home_item_show (GtkWidget *widget)
{
    /* Do not call the gtk show. keep the gtk window hidden */
    (void) widget; // unused
}

static void
qt_abstract_home_item_class_init (QtAbstractHomeItemClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = qt_abstract_home_item_finalize;

    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    widget_class->realize = qt_abstract_home_item_realize;
    widget_class->show = qt_abstract_home_item_show;

    g_type_class_add_private (klass, sizeof (QtAbstractHomeItemPrivate));
}

/*
static void
qt_abstract_home_item_class_finalize (QtAbstractHomeItemClass *klass)
{
}
*/

// client event with hildon settings
// client notify event...


#define HD_PLUGIN_LOADER_QT_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), HD_TYPE_PLUGIN_LOADER_QT, HDPluginLoaderQtPrivate))

G_DEFINE_TYPE (HDPluginLoaderQt, hd_plugin_loader_qt, HD_TYPE_PLUGIN_LOADER);

struct _HDPluginLoaderQtPrivate
{
  GHashTable *registry;
};


static GObject *
hd_plugin_loader_qt_load (HDPluginLoader  *loader,
                               const gchar     *plugin_id,
                               GKeyFile        *keyfile,
                               GError         **error)
{
    GObject *object = NULL;

    g_return_val_if_fail (loader, NULL);

    if (!keyfile)
    {
        g_set_error (error,
                hd_plugin_loader_error_quark (),
                HD_PLUGIN_LOADER_ERROR_KEYFILE,
                "A keyfile required to load plugins");

        return NULL;
    }

    /* construct a new proxy object */
    object = g_object_new (QT_TYPE_ABSTRACT_HOME_ITEM,
            "plugin-id", plugin_id,
            NULL);

    /* --- get the executable path */
    {
        QtAbstractHomeItemPrivate *priv = QT_ABSTRACT_HOME_ITEM (object)->priv;
        GError *keyfile_error = NULL;
        gchar *executable_file = NULL;

        executable_file = g_key_file_get_string (keyfile,
                HD_PLUGIN_CONFIG_GROUP,
                HD_PLUGIN_CONFIG_KEY_PATH,
                &keyfile_error);
        g_strstrip (executable_file);

        if (keyfile_error)
        {
            g_propagate_error (error, keyfile_error);
            return NULL;
        }

        if (g_path_is_absolute (executable_file))
        {
            priv->executable_path = executable_file;
        }
        else
        {
            priv->executable_path = g_build_filename (HD_PLUGIN_MODULE_PATH,
                    executable_file,
                    NULL);

            g_free (executable_file);
        }
    }

    return object;
}

    static void
hd_plugin_loader_qt_finalize (GObject *loader)
{
    HDPluginLoaderQtPrivate *priv;

  g_return_if_fail (loader != NULL);
  g_return_if_fail (HD_IS_PLUGIN_LOADER_QT (loader));

  priv = HD_PLUGIN_LOADER_QT (loader)->priv;

  if (priv->registry != NULL)
    {
      g_hash_table_destroy (priv->registry);
      priv->registry = NULL;
    }

  G_OBJECT_CLASS (hd_plugin_loader_qt_parent_class)->finalize (loader);
}

static void
hd_plugin_loader_qt_init (HDPluginLoaderQt *loader)
{
  loader->priv = HD_PLUGIN_LOADER_QT_GET_PRIVATE (loader);

  loader->priv->registry = g_hash_table_new_full (g_str_hash,
                                                  g_str_equal,
                                                  (GDestroyNotify) g_free,
                                                  NULL);
}

static void
hd_plugin_loader_qt_class_init (HDPluginLoaderQtClass *class)
{
    GObjectClass *object_class;
    HDPluginLoaderClass *loader_class;

    object_class = G_OBJECT_CLASS (class);
    object_class->finalize = hd_plugin_loader_qt_finalize;

    // widget_class = GTK_WIDGET_CLASS (class);

    loader_class = HD_PLUGIN_LOADER_CLASS (class);
    loader_class->load = hd_plugin_loader_qt_load;

    g_type_class_add_private (object_class, sizeof (HDPluginLoaderQtPrivate));
}

G_MODULE_EXPORT gchar *
hd_plugin_loader_module_type (void)
{
  return "qt";
}

G_MODULE_EXPORT HDPluginLoader *
hd_plugin_loader_module_get_instance (void)
{
  return g_object_new (HD_TYPE_PLUGIN_LOADER_QT, NULL);
}



