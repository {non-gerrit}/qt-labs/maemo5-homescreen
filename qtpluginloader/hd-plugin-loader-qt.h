/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef __HD_PLUGIN_LOADER_QT_H__
#define __HD_PLUGIN_LOADER_QT_H__

#include <libhildondesktop/libhildondesktop.h>

G_BEGIN_DECLS

#define QT_LOADER_EXECUTE_ERROR 1

#define QT_TYPE_ABSTRACT_HOME_ITEM            (qt_abstract_home_item_get_type ())
#define QT_ABSTRACT_HOME_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), QT_TYPE_ABSTRACT_HOME_ITEM, QtAbstractHomeItem))
#define QT_ABSTRACT_HOME_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), QT_TYPE_ABSTRACT_HOME_ITEM, QtAbstractHomeItemClass))
#define QT_IS_ABSTRACT_HOME_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), QT_TYPE_ABSTRACT_HOME_ITEM))
#define QT_IS_ABSTRACT_HOME_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), QT_TYPE_ABSTRACT_HOME_ITEM))
#define QT_ABSTRACT_HOME_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), QT_TYPE_ABSTRACT_HOME_ITEM, QtAbstractHomeItemClass))

typedef struct _QtAbstractHomeItem        QtAbstractHomeItem;
typedef struct _QtAbstractHomeItemClass   QtAbstractHomeItemClass;
typedef struct _QtAbstractHomeItemPrivate QtAbstractHomeItemPrivate;

struct _QtAbstractHomeItem
{
  HDHomePluginItem       parent;

  QtAbstractHomeItemPrivate       *priv;
};

struct _QtAbstractHomeItemClass
{
  HDHomePluginItemClass  parent;
};

GType qt_abstract_home_item_get_type (void);


typedef struct _HDPluginLoaderQt HDPluginLoaderQt;
typedef struct _HDPluginLoaderQtClass HDPluginLoaderQtClass;
typedef struct _HDPluginLoaderQtPrivate HDPluginLoaderQtPrivate;

#define HD_TYPE_PLUGIN_LOADER_QT            (hd_plugin_loader_qt_get_type ())
#define HD_PLUGIN_LOADER_QT(obj)		 (G_TYPE_CHECK_INSTANCE_CAST ((obj), HD_TYPE_PLUGIN_LOADER_QT, HDPluginLoaderQt))
#define HD_IS_PLUGIN_LOADER_QT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), HD_TYPE_PLUGIN_LOADER_QT))
#define HD_PLUGIN_LOADER_QT_CLASS(klass)	 (G_TYPE_CHECK_CLASS_CAST ((klass), HD_TYPE_PLUGIN_LOADER_QT_CLASS, HDPluginLoaderQtClass))
#define HD_IS_PLUGIN_LOADER_QT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), HD_TYPE_PLUGIN_LOADER_QT_CLASS))
#define HD_PLUGIN_LOADER_QT_GET_CLASS(obj)	 (G_TYPE_INSTANCE_GET_CLASS ((obj), HD_TYPE_PLUGIN_LOADER_QT, HDPluginLoaderQtClass))

struct _HDPluginLoaderQt
{
  HDPluginLoader parent;

  HDPluginLoaderQtPrivate *priv;
};

struct _HDPluginLoaderQtClass
{
  HDPluginLoaderClass parent_class;
};

GType  hd_plugin_loader_qt_get_type  (void);

G_END_DECLS

#endif /* __HD_PLUGIN_LOADER_QT_H__ */
