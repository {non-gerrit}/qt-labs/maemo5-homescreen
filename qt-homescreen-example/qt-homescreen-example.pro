TEMPLATE = app

SOURCES += main.cpp
HEADERS += testwidget.h

include(../qmaemo5homescreenadaptor/qmaemo5homescreenadaptor.pri)

desktop.path = /usr/share/applications/hildon-home
desktop.files = qt-homescreen-example.desktop

target.path = /usr/lib/hildon-desktop
INSTALLS += target desktop
