/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTWIDGET_H
#define TESTWIDGET_H

#include <QtGui/qlabel.h>
#include <QtGui/qinputdialog.h>
#include <QtGui/qpainter.h>

class TestWidget : public QLabel
{
    Q_OBJECT

public:
    TestWidget()
        : QLabel(tr("Hello World"))
    {
        setAlignment(Qt::AlignCenter);
        setAttribute(Qt::WA_TranslucentBackground);
    }

    QSize sizeHint() const
    {
        return 2 * QLabel::sizeHint();
    }

public slots:
    void showSettingsDialog()
    {
        bool isOk;
        QString newText = QInputDialog::getText(this, tr("New Text"),
                tr("Please enter a new text:"), QLineEdit::Normal,
                text(), &isOk);
        if (isOk)
            setText(newText);
    }

protected:
    void paintEvent(QPaintEvent *event)
    {
        QPainter p(this);
        p.setBrush(QColor(0, 0, 0, 128));
        p.setPen(Qt::NoPen);
        p.drawRoundedRect(rect(), 25, 25);
        p.end();

        QLabel::paintEvent(event);
    }
};

#endif
